package com.company;

public class Lamborghini extends Car{

    public LamborghiniType model;

    public Lamborghini(){

    }



    public LamborghiniType getModel() {
        return model;
    }

    public void setModel(LamborghiniType model) {
        this.model = model;
    }

    public Lamborghini(LamborghiniType model){

    }

    public Lamborghini(LamborghiniType model,String color, Double engineVolume){
        super(color,engineVolume);
        setModel(model);
    }
//
////    protected void builder(){
////        System.out.println(getColor()+getEngineVolume());
////    }
    @Override
    public String toString() {
        return "lamborghini{" +
                "model='" + model + '\''+model+'\'' +super.toString()+
                '}';
    }
}
