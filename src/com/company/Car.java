package com.company;

public abstract class Car implements ICar{

    private Double engineVolume;
    private String color;

    public Car() {

    }

    public Car(String color, Double engineVolume){
        this.color=color;
        this.engineVolume=engineVolume;
    }

    public Double getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(Double engineVolume) {
        this.engineVolume = engineVolume;
    }

    //protected abstract void builder();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return  " volume=" + engineVolume +
                ", color='" + color + '\'' +
                '}';
    }






}
