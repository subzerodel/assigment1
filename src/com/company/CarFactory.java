package com.company;

public class CarFactory {
    CarBuilder carBuilder;
    LamborghiniType lamborghiniType;
    FordType fordType;
    Lamborghini lambo;
    Ford ford;
    if(carBuilder.getBrand()=="Lamborghini"){
        switch (lamborghiniType){
            case AVENTADOR:
                lambo = new Lamborghini(lamborghiniType.AVENTADOR);
                break;
            case SIAN:
                lambo = new Lamborghini(lamborghiniType.SIAN);
                break;
            case VENENO:
                lambo = new Lamborghini(LamborghiniType.VENENO);
                break;
            default:
                System.out.println("The requested car was unfortunately not available.");
                lambo = null;
        }
    }else {
        switch (fordType) {
            case MUSTANG:
                ford = new Ford(FordType.MUSTANG);
                break;
            case FOCUS:
                ford = new Ford(FordType.FOCUS);
                break;
            case USA:
                ford = new Ford(FordType.USA);
                break;
            default:
                System.out.println("The requested car was unfortunately not available.");
                ford = null;
        }
    }
}
