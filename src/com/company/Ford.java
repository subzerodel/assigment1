package com.company;

public class Ford extends Car {
    public FordType model;

    public Ford(){

    }

    public FordType getModel() {
        return model;
    }

    public void setModel(FordType model) {
        this.model = model;
    }

    public Ford(FordType model, String color, Double engineVolume ) {
        super(color,engineVolume);
        setModel(model);
    }

    public Ford(FordType model){
        this.model=model;
    }

    @Override
    public String toString() {
        return "Ford{" +
                "model='" + model + '\'' +super.toString()+
                '}';
    }
}
