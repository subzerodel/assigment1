package com.company;

public class CarBuilder {

    private String brand;

    CarBuilder(String brand){
        this.brand=brand;
    }

    public String getBrand(){
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    //    public Lamborghini lamborghini;
//    public Ford ford;
//
//    public Object buildLamborghini(LamborghiniType model, String color, double engineVolume) {
//        lamborghini = new Lamborghini(model, color, engineVolume);
//
//        return lamborghini;
//    }
//
//    public Object buildFord(FordType model, String color, double engineVolume) {
//        ford = new Ford(model, color, engineVolume);
//        return ford;
//    }
//    public static Car getFactory(CarType carType){
//        switch (carType){
//            case SMALL:
//                return new SmallCar(carType);
//            case SEDAN:
//                return new Sedan(carType);
//            case LUXURY:
//                return new LuxuryCar(carType);
//            default:
//                return null;
//        }
//    }

}
